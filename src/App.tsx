import React from 'react';
import './App.css';
import InputField from './components/InputField';

/* TYPE -alias e.g. type and interface


let name: string;
let name: any; NOT RECOMMENDED
unknown type
letpersonName:unknown; RECOMMENDED same with anytype;
union OR
let age: number | string;
let isStudent: boolean;
let hobbies: string[];
tuple-containes two different data types
let role: [number,string];

defining a function

(parameter:type) => what it returns
void returns undefined
never does not return anything 
let printName: (name:string) => never;

function printName (name:string) {
  console.log(name);
}

printName("Piyush");


declares the type of an Object
if property is optional add ?


interface Person  {
  name: string;
  age?: number;
}

interface Person extends X {
  name: string;
  age?: number;
}

interface Guy extends Person{
  profession: string;
}

type X = Person & {
  a: string;
  b: number;
}

type X = {
  a: string;
  b: number;
}
type Y = X & {
  c: string;
  d: number;
}

missing a and b properties
let y: Y = {
  c: 'hello',
  d: 32
}
let person: Person ={
  name: 'Piyush'

};


// array of objects
let lotsOfPeople: Person[]; */

// REACT.FC - functional component
const App: React.FC = () => {
  
  return (
    <div className="App">
      <span className='heading'>toDoit</span>

      <InputField />

    </div>
  );
}

export default App;
